/*globals _, $*/

(function (context) {
	'use strict';

/*******************************************************************************
* Yeah, we could place the template in a handlebars object, but that's an
* unnecessary overhead layer for something that won't change.
*******************************************************************************/

	var TEMPLATE = '<ul class="expressionist-filters"></ul><div class="expressionist-sources"><div class="expressionist-search"><i class="icon"></i><i class="clear"></i><div contenteditable></div></div><ul class="expressionist-table"></ul></div><div class="expressionist-result"></div>';

	var FILTER = '<li class="filter"><div class="filter-header"><button class="add">+</button><button class="remove">-</button><select><option value="and">AND</option><option value="or">OR</option></select></div><ul><li class="drop-target"></li></ul></li>';

/*******************************************************************************
* CLASS
*******************************************************************************/

	function Expressionist ($container, options) {
		this.$container = $container;
		this.options = options || {};
		this.preInstall = {};
		this.install();
		this.buildSourcesTable();
		this.generateFormula();
	}

	Expressionist.prototype = {
		options: undefined,
		preInstall: undefined,
		searchTimeoutID: undefined,

		$container: undefined,
		$search: undefined,
		$table: undefined,
		$filters: undefined,

		install: function () {
			this.$container
			.addClass('expressionist')
			.html(TEMPLATE)
			.data('expressionist', this);

			// cache css position setting
			// set to relative if not already that or absolute
			var position = this.$container.css('position');
			this.preInstall.position = position;

			switch (position) {
			case 'absolute':
			case 'relative':
				break;
			default:
				this.$container.css('position', 'relative');
				break;
			}

			//
			this.$search = this.$container.find('.expressionist-search')
			.on('keydown', _.bind(this.onSearchFieldKeyDown, this))
			.on('keyup', _.bind(this.onSearchFieldKeyUp, this));

			this.$search.find('.clear')
			.on('click', _.bind(this.onSearchFieldCleared, this));

			//
			this.$table = this.$container.find('.expressionist-table')
			.on('click', _.bind(this.onFolderClick, this));

			//
			this.$filters = this.$container.find('.expressionist-filters');
		},

		uninstall: function () {
			clearTimeout(this.searchTimeoutID);

			//
			this.$container
			.removeClass('expressionist')
			.css('position', this.preInstall.position);

			//
			this.$search
			.off('keydown', this.onSearchFieldKeyDown)
			.off('keyup', this.onSearchFieldKeyUp);

			this.$search.find('.clear')
			.off('click', _.bind(this.onSearchFieldCleared, this));

			//
			this.$table.find('li > div')
			.off('click', this.onFolderClick);
		},

/*******************************************************************************
* METHODS
*******************************************************************************/

		buildSourcesTable: function () {
			var sources = this.options.sources || {};
			_.each(sources || [], function (folder) {
				this.generateSourcesFolder(folder, this.$table);
			}, this);

			//
			this.$table.find('li.folder > div')
			.on('click', this.onFolderClick);

			//
			this.$table.find('li.attribute > div').draggable({
				appendTo: $('body'),
				containment: 'window',
				cursorAt: {left: -10, top: 10},
				drag: _.bind(this.onFilterDragging, this),
				helper: 'clone',
				revert: 'invalid',
				start: _.bind(this.onFilterDragged, this),
				stop: function () {
					//$('.drop-target').hide();
				},
				zIndex: 9999
			});

			$('<li>')
			.addClass('instructions')
			.html('Drag attributes to the right.')
			.appendTo(this.$table);

			//
			this.$filters.html(FILTER);

			//
			this.$filters.find('.filter ul').droppable({
				accept: '.ui-draggable',
				activeClass: 'droppable',
				drop: _.bind(this.onFilterDropped, this),
				greedy: true,
				tolerance: 'pointer'
			}).sortable({
				axis: 'y',
				containment: 'parent',
				items: '> li',
				tolerance: 'pointer',
				stop: _.bind(this.onFilterSortStopped, this)
			});

			//
			this.$filters.find('.filter .filter-header > button.add')
			.click(_.bind(this.onNewFilter, this));

			this.$filters.find('.filter .filter-header > button.remove')
			.click(_.bind(this.onRemoveFilter, this));

			this.$filters.find('.filter select')
			.change(_.bind(this.generateFormula, this));
		},

		generateSourcesFolder: function (data, $folder) {
			var $li = $('<li>')
			.appendTo($folder);

			$('<div>')
			.addClass('kind-' + data.kind)
			.appendTo($li)
			.html(data.label)
			.data('info', data);

			if (data.type === 'folder') {
				$li.addClass('folder');

				var $ul = $('<ul>')
				.appendTo($li);

				_.each(data.items || [], function (data) {
					this.generateSourcesFolder(data, $ul);
				}, this);
			} else {
				$li.addClass('attribute');
			}
		},

/*******************************************************************************
* FOLDER
*******************************************************************************/

		onFolderClick: function (e) {
			var $folder = $(e.currentTarget).parent();
			$folder.toggleClass('open');
		},

		onFilterDragged: function (e, ui) {
			var $attribute = $(e.currentTarget);
			ui.helper.data('info', $attribute.data('info'));
		},

		// highlight only the nearest
		onFilterDragging: function (e, ui) {
			// var $droppableToShow;
			// var shortestDistance = Number.MAX_VALUE;

			// var x1 = e.clientX;
			// var y1 = e.clientY;

			// $('.droppable .drop-target')
			// .each(function () {
			// 	var $this = $(this).show();
			// 	var position = $this.offset();
			// 	var y2 = position.top + ($this.height() / 2);
			// 	var x2 = position.left;

			// 	//$this.hide();

			// 	var distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
			// 	if (distance > shortestDistance) { return; }

			// 	$droppableToShow = $this;
			// 	shortestDistance = distance;
			// });

			// $droppableToShow.show();
		},

		onFilterDropped: function (e, ui) {
			e.preventDefault();
			e.stopPropagation();

			var $dropTarget = $(e.target).children('.drop-target');
			var $attribute = ui.helper;

			var data = $attribute.data('info');
			data.operator = '==';
			data.value = '';

			var $filter = $('<li>')
			.addClass('attribute')
			.insertBefore($dropTarget);

			$filter.data(data);

			$('<strong>')
			.appendTo($filter)
			.html(data.label);

			_.each(data, function (value, attr) {
				$filter.attr('data-' + attr, value);
			});

			this.renderFilterToText($filter);

			// update ui
			//$('.drop-target').hide();
			this.generateFormula();

			return false;
		},

		onFilterSortStopped: function (e, ui) {
			var $filters = $(e.target);
			$filters.find('.drop-target')
			.appendTo($filters);

			//
			this.generateFormula();
		},

		onFilterDeleted: function (e) {
			var $attribute = $(e.target).parent();

			// scrub events

			//
			$attribute.remove();

			// update ui
			this.generateFormula();
		},

		onFilterEdit: function (e) {
			this.renderFilterToControl($(e.target).parent());
		},

		onFilterSave: function (e) {
			var $attribute = $(e.target).parent();
			var data = $attribute.data();

			// value
			var value = $attribute.find('.value.term').val();
			data.value = this.processControlValue(data.kind, value);

			// betweenValue
			data.betweenValue = undefined;

			var operator = data.operator;
			if (operator === '<>' || operator === '><') {
				var betweenValue = $attribute.find('.between-value.term').val();
				data.betweenValue = this.processControlValue(data.kind, betweenValue);
			}

			//
			$attribute.data(data);

			// redraw
			this.renderFilterToText($attribute);

			//
			this.generateFormula();
		},

		onNewFilter: function (e) {
			var $filter = $(e.target).closest('.filter');
			var $parent = $filter.parent();

			$filter = $filter.clone();
			$filter.find('ul li:not(.drop-target)').remove();

			$filter.appendTo($parent);

			//
			$filter.find('ul').droppable({
				accept: '.ui-draggable',
				activeClass: 'droppable',
				drop: _.bind(this.onFilterDropped, this),
				greedy: true,
				tolerance: 'pointer'
			}).sortable({
				axis: 'y',
				containment: 'parent',
				items: '> li',
				tolerance: 'pointer',
				stop: _.bind(this.onFilterSortStopped, this)
			});

			//
			$filter.find('.filter-header > button.add')
			.click(_.bind(this.onNewFilter, this));

			$filter.find('.filter-header > button.remove')
			.click(_.bind(this.onRemoveFilter, this));

			$filter.find('select')
			.change(_.bind(this.generateFormula, this));
		},

		onFilterIndent: function (e) {
			var $attribute = $(e.target).parent();

			// test if we should indent

			//
			var $filter = $attribute.closest('.filter').clone()
			.insertBefore($attribute);

			// add top-level button events
			$filter.find('.filter-header > button.add')
			.off('click')
			.click(_.bind(this.onNewFilter, this));

			$filter.find('.filter-header > button.remove')
			.off('click')
			.click(_.bind(this.onRemoveFilter, this));

			$filter.find('select')
			.off('click')
			.change(_.bind(this.generateFormula, this));

			// configure drop/sortable list
			var $filters = $filter.children('ul');
			$filters.find('.attribute, .filter').remove();

			$filters.prepend($attribute);

			$filters
			.droppable({
				accept: '.ui-draggable',
				activeClass: 'droppable',
				greedy: true,
				drop: _.bind(this.onFilterDropped, this),
			 tolerance: 'pointer'
			}).sortable({
				axis: 'y',
				containment: 'parent',
				items: '> li',
				tolerance: 'pointer',
				stop: _.bind(this.onFilterSortStopped, this)
			});

			// update ui
			this.generateFormula();
		},

		onRemoveFilter: function (e) {
			$(e.target).closest('.filter').remove();

			//
			this.generateFormula();
		},

		onFilterOperatorChanged: function (e) {
			var $select = $(e.target);
			var operator = $select.val();

			var $attribute = $select.parent()
			.attr('data-operator', operator);

			var data = $attribute.data();
			data.operator = operator;

			$attribute.data(data);
		},

		renderFilterToText: function ($attribute) {
			var data = $attribute.data();

			// scrub events

			//
			$attribute.empty();

			// delete button
			$('<button>')
			.addClass('term delete')
			.appendTo($attribute)
			.on('click', _.bind(this.onFilterDeleted, this))
			.html('[x]');

			// indent button
			$('<button>')
			.addClass('term indent')
			.on('click', _.bind(this.onFilterIndent, this))
			.appendTo($attribute)
			.html('&gt;&gt;');

			// edit button
			$('<button>')
			.addClass('term edit')
			.on('click', _.bind(this.onFilterEdit, this))
			.appendTo($attribute)
			.html('[edit]');

			// attribute
			$('<div>')
			.addClass('term key')
			.appendTo($attribute)
			.html(data.label);

			// operator
			var operator = data.operator;
			this.renderOperatorAsString($attribute, data.kind, operator);

			// value
			this.renderDataToStringTerm($attribute, data.kind, data.value)
			.addClass('value');

			// skip "and" and "value2" if operator is not between
			if (operator !== '<>' && operator !== '><') { return; }

			// and
			this.renderAndTerm($attribute);

			// betweenValue
			this.renderDataToStringTerm($attribute, data.kind, data.betweenValue)
			.addClass('between-value');
		},

		renderOperatorAsString: function ($parent, kind, operator) {
			var string;
			switch (kind) {
			case 'number':
			case 'string':
			case 'date':
				switch (operator) {
				case '==': string = 'equals'; break;
				case '!=': string = 'doesn\'t equal'; break;
				case '>': string = 'is greater than'; break;
				case '>=': string = 'is greater than or equal to'; break;
				case '<': string = 'is less than'; break;
				case '<=': string = 'is less than or equal to'; break;
				case '><': string = 'is between'; break;
				case '<>': string = 'is not beween'; break;
				}
				break;
			case 'boolean':
				switch (operator) {
				case '==': string = 'is'; break;
				case '!=': string = 'isn\'t'; break;
				}
				break;
			}

			return $('<div>')
			.addClass('term operator')
			.appendTo($parent)
			.html(string);
		},

		renderAndTerm: function ($parent, container) {
			container = container || 'span';

			return $('<' + container + '>')
			.addClass('term and')
			.appendTo($parent)
			.html('and');
		},

		renderFilterToControl: function ($attribute) {
			var data = $attribute.data();

			// scrub events

			//
			$attribute.empty();
			$attribute.attr('data-operator', data.operator);

			// delete button
			$('<button>')
			.addClass('term delete')
			.appendTo($attribute)
			.on('click', _.bind(this.onFilterDeleted, this))
			.html('[x]');

			// indent button
			$('<button>')
			.addClass('term indent')
			.on('click', _.bind(this.onFilterIndent, this))
			.appendTo($attribute)
			.html('&gt;&gt;');

			// edit button
			$('<button>')
			.addClass('term edit')
			.on('click', _.bind(this.onFilterSave, this))
			.appendTo($attribute)
			.html('[save]');

			// key
			this.renderKeyTerm($attribute, data.label);

			// operator
			this.renderOperatorControl($attribute, data.kind, data.operator);

			// value
			this.renderValueControl($attribute, data.kind, data.value)
			.addClass('value');

			// and
			this.renderAndTerm($attribute, 'div');

			// betweenValue
			this.renderValueControl($attribute, data.kind, data.betweenValue)
			.addClass('between-value');
		},

		renderKeyTerm: function ($parent, label, container) {
			container = container || 'div';

			return $('<' + container + '>')
			.addClass('term key')
			.appendTo($parent)
			.html(label);
		},

		renderOperatorControl: function ($parent, kind, currentOperator) {
			var operators = {
				'==': 'equals',
				'!=': 'doesn\'t equal',
			};

			switch (kind) {
			case 'boolean':
				operators = {
					'==': 'equals',
					'!=': 'doesn\'t equal'
				};
				break;
			case 'string':
				operators = $.extend(operators, {
					'^=': 'starts with',
					'$=': 'ends with',
					'%=': 'contains'
				});
				break;
			case 'date':
			case 'number':
				operators = $.extend(operators, {
					'><': 'is between',
					'<>': 'is not between',
					'>': 'is greater than',
					'>=': 'is greater than or equal to',
					'<': 'is less than',
					'<=': 'is less than or equal to'
				});
				break;
			}

			var $select = $('<select>')
			.addClass('term operator')
			.appendTo($parent)
			.change(_.bind(this.onFilterOperatorChanged, this));

			_.each(operators, function (string, operator) {
				var $option = $('<option>')
				.attr('value', operator)
				.appendTo($select)
				.html(string);

				if (operator === currentOperator) {
					$option.attr('selected', 'selected');
				}
			});

			return $select;
		},

		renderValueControl: function ($parent, kind, value) {
			var $control;
			switch(kind) {
			case 'string':
				$control = $('<input>')
				.addClass('term')
				.val(value || '');
				break;

			case 'number':
				$control = $('<input>')
				.addClass('term')
				.val(value || 0);
				break;

			case 'date':
				$control = $('<input>')
				.addClass('term')
				.attr('readonly', 'readonly')
				.val(value || '')
				.datepicker();
				break;

			case 'boolean':
				$control = $('<select>')
				.addClass('term value');

				$('<option>')
				.attr('value', 1)
				.appendTo($control)
				.text('TRUE');

				$('<option>')
				.attr('value', 0)
				.appendTo($control)
				.text('FALSE');

				$control[0].selectedIndex = (value) ? 0 : 1;
				break;
			}

			return $control
			.attr('data-kind', kind)
			.appendTo($parent);
		},

		renderDataToStringTerm: function ($parent, kind, value) {
			var valueString = this.renderDataToString(kind, value);

			var $value = $('<div>')
			.addClass('term')
			.addClass(kind)
			.appendTo($parent)
			.html(valueString);

			return $value;
		},

		renderDataToString: function (kind, value) {
			switch (kind) {
			case 'number': return value || 0;
			case 'string': return '"' + value + '"';
			case 'boolean': return (value === 1) ? 'TRUE' : 'FALSE';
			case 'date':
				if (!value) {
					return 'NOW';
				} else {
					return value || '';
				}
				break;
			}
		},

		processControlValue: function (kind, value) {
			switch (kind) {
			case 'string':
				return $.trim(value);
			case 'number':
				return parseFloat(value);
			case 'date':
				return $.trim(value);
			case 'boolean':
				return (value > 0) ? 1 : 0;
			}
		},

/*******************************************************************************
* SEARCH
*******************************************************************************/

		onSearchFieldKeyDown: function (e) {
			if (this.shouldICancelThisKeyUpEvent(e)) { return; }
		},

		onSearchFieldKeyUp: function (e) {
			if (this.shouldICancelThisKeyUpEvent(e)) { return; }

			var search = this.$search.text();

			clearTimeout(this.searchTimeoutID);
			this.searchTimeoutID = setTimeout(_.bind(this.onSearch, this), 250, search);
		},

		onSearchFieldCleared: function (e) {
			this.$search.find('div').html('');
			this.onSearch('');
		},

		onSearch: function (searchTerm) {
			searchTerm = $.trim(searchTerm);

			//
			var $table = this.$table;
			$table.find('.search-results').remove();

			var $clearButton = this.$search.find('.clear').hide();

			// if no search, show folders
			var $folders = $table.find('.folder').hide();
			if (!searchTerm.length) { return $folders.show(); }

			//
			$clearButton.show();

			//
			var searchTerms = searchTerm
			.toLowerCase()
			.replace(/\s+/g, ' ')
			.split(' ');

			var $searchResults = $('<li>')
			.addClass('search-results folder open')
			.prependTo($table);

			$('<div>')
			.html('Search Results')
			.appendTo($searchResults);

			var $searchResultsList = $('<ul>')
			.appendTo($searchResults);

			$table.find('.attribute > div').each(function () {
				var $this = $(this);
				var data = $this.data('info');
				var label = data.label.toLowerCase();

				_.some(searchTerms, function (term) {
					if (label.indexOf(term) < 0) { return false; }

					var $li = $('<li>')
					.addClass('attribute')
					.appendTo($searchResultsList);

					var $clone = $this.clone()
					.prependTo($li);

					$clone.data('info', data);

					return true;
				});
			});

			$searchResultsList.find('.attribute > div')
			.draggable({
				appendTo: $('body'),
				containment: 'window',
				cursorAt: {left: -10, top: 10},
				drag: _.bind(this.onFilterDragging, this),
				helper: 'clone',
				revert: 'invalid',
				start: _.bind(this.onFilterDragged, this),
				stop: function () {
					//$('.drop-target').hide();
				},
				zIndex: 9999
			});
		},

		shouldICancelThisKeyUpEvent: function (e) {
			// disable return key
			if (e.keyCode === 13) {
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
		},

/*******************************************************************************
* FORMULA GENERATION
*******************************************************************************/

		generateFormula: function () {
			var self = this;
			var $result = $('.expressionist-result').empty();

			var $filters = this.$filters.children('.filter');

			if (!this.$filters.find('.attribute').length) {
				return $('<span>')
				.html('No parameters.')
				.appendTo($result);
			}

			$filters.each(function (index) {
				var $filter = $(this);

				if (index > 0) {
					$('<span>')
					.html('AND')
					.appendTo($result);
				}

				var $and = $('<span>')
				.appendTo($result);

				var value = $filter.find('select').val();
				self.generateFilter($filter, $and, value);
			});
		},

		generateFilter: function ($filter, $parent, operatorType) {
			var self = this;
			$filter.find('> ul > li:not(.drop-target)').each(function (index) {
				var $attribute = $(this);

				if (index > 0) {
					$('<span>')
					.html((operatorType || '').toUpperCase())
					.appendTo($parent);
				}

				if ($attribute.hasClass('filter')) {
					var $content = $('<span>');

					$('<span>')
					.addClass('deeper')
					.append('(')
					.append($content)
					.append(')')
					.appendTo($parent);

					var value = $attribute.find('select').val();
					return self.generateFilter($attribute, $content, value);
				}

				var data = $attribute.data();

				self.renderFormulaOperation($parent, data);
			});
		},

		renderFormulaOperation: function ($parent, data) {
			var value = this.renderDataToString(data.kind, data.value);
			var betweenValue;

			var string;
			switch (data.operator) {
			case '<>':
				betweenValue = this.renderDataToString(data.kind, data.betweenValue);
				string = data.key + ' >= ' + value + ' < ' + betweenValue;
				break;
			case '><':
			betweenValue = this.renderDataToString(data.kind, data.betweenValue);
				string = data.key + ' < ' + value + ' > ' + betweenValue;
				break;
			default:
				string = data.key + ' ' + data.operator + ' ' + value;
				break;
			}

			return $('<span>')
			.appendTo($parent)
			.html(string);
		}

/*******************************************************************************
* /CLASS
*******************************************************************************/
	};

/*******************************************************************************
* DEFAULTS
*******************************************************************************/

	var defaults = {
	};

/*******************************************************************************
* PLUGIN
*******************************************************************************/

	context.$.fn.expressionist = function (methodName, options) {
		if (methodName !== 'install') {
			return this.each(function () {
				var $this = $(this);
				var expressionist = $this.data('expressionist');

				if (typeof expressionist[methodName] !== 'function') {
					return console.error('Not an expressionist method:', methodName);
				}

				expressionist[methodName](options);
			});
		}

		// install
		options = $.extend(defaults, options);
		return this.each(function () {
			new Expressionist($(this), options);
		});
	};


/*******************************************************************************
* AMD
*******************************************************************************/

	if (typeof define === 'function' && define.amd) {
		define(function() { return true; });
	}

/*******************************************************************************
* /END
*******************************************************************************/
})(window || this);
