/*globals module*/
/*jshint node:true, camelcase:false*/

'use strict';

module.exports = function (grunt) {
	// we could do this...
	// require('load-grunt-tasks')(grunt);
	// but it takes so long to load

	grunt.loadNpmTasks('grunt-bump');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-env');

	var pkg = grunt.file.readJSON('package.json');

	grunt.initConfig({
		pkg: pkg,

		env: {
			development: {
				DEVELOPMENT: true
			},

			production: {
			}
		},

		less: {
			development: {
				options: {
					paths: ['style'],
				},
				files: {
					'main.css': 'style/main.less'
				}
			},
			production: {
				options: {
					paths: ['style'],
					compress: true,
					cleancss: true
				},
				files: {
					'dist/expressionist.css': 'style/expressionist.less'
				}
			}
		},

        connect: {
            options: {
                livereload: true,
                keepalive: true
            },
            development: {
                options: {
                    base: '',
	                port: 3000,
	                hostname: '*'
                }
            }
        },

		watch: {
			configFiles: {
				files: 'gruntfile.js',
				options: {
					reload: true
				}
			},

			thingsToReload: {
				files: [
					'index.html',
					'expressionist.js'
				],
				options: {
					livereload: true,
					reload: true
				}
			},

			thingsToSideLoad: {
				files: ['main.css'],
				options: {
					livereload: true,
					reload: false
				}
			},

			less: {
				files: 'style/*.less',
				tasks: ['less:development'],
				options: {
					reload: false
				}
			}
		},

		concurrent: {
			development: {
				tasks: ['watch', 'connect:development'],
				options: {
					logConcurrentOutput: true
				}
			}
		},

		clean: {
			production: {
				files: [{
					dot: true,
					src: ['dist']
				}]
			}
		},

		bump: {
			options: {
				files: ['package.json', 'bower.json'],
				commit: false,
				createTag: false,
				push: false
			}
		}
	});

	grunt.registerTask('default', [
		'env:development',
		'less:development',
		'concurrent:development'
	]);
};
